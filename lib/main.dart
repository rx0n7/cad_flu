import 'package:flutter/material.dart';
import 'package:z_est/src/JokenHome.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: JokenHome(),
  ));
}
